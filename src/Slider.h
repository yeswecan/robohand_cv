//
//  Slider.h
//  robohandCV
//
//  Created by zebra on 17/09/14.
//
//

#ifndef robohandCV_Slider_h
#define robohandCV_Slider_h

class Slider: public Object {
public:
    Slider(string name, ofPoint pos, ofPoint size, DragDispatcher *dragD) : Object(name, pos, size) {
        draw = [&](Object *o) {
            ofSetColor(0);
            ofRect(0,0,o->size.x, o->size.y);
            if (!downOnMe)
                ofSetColor(200, 100);
            else
                ofSetColor(200, 0, 0, 100);
            ofRect(0,0,o->size.x * value,o->size.y);
            
            if (showCaption) {
                ofSetColor(255);
                string report = ofToString((int)(value * scale)) + captionAddon;
                ofRectangle stringSize = font->getStringBoundingBox(report, 0, 0);
                font->drawString(report, o->size.getAnimated().x/2 - stringSize.width/2, stringSize.height + 5);
            }
        };
        
        function<bool(Object *o)> sliderSet = [&](Object *o)->bool {
            //ofLog() << o->lastTouchedPosition;
            value = o->lastTouchedPosition.x / o->size.x;
            downOnMe = true;
            return true;
        };
        touchDownC = sliderSet;

        update = [&](Object *o) {
            if ((!touchingNow) && (downOnMe)) {
                downOnMe = false;
            }
            if (downOnMe)
                value = ofClamp( ((o->touchPoint.x - o->transformedPosition.x) / o->size.x), 0, 1);;
        };
        
    };
    
    ofTrueTypeFont *font;
    
    bool showCaption = true;
    string captionAddon = " pix";
    float scale = 50;
    
    bool downOnMe = false;

    float value = 0.5;

private:
};

#endif
