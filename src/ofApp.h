#pragma once

#include "ofMain.h"
#include "ofxUIGraph.h"
#include "Slider.h"

#define $ canvas->getChild

#define PICKER_COUNT 6

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
        void exit(ofEventArgs & args) {
            saveSettings();
        }
    
    void showPanel() {
        panel->size.animateTo(ofPoint(300, 500), 600);
        panel->data["appeared"] = 1;
        ofLog() << "showing panel";
    }
    
    void hidePanel() {
        panel->size.animateTo(ofPoint(300, 100), 600);
        panel->data["appeared"] = 0;
        input->data["focused"] = 0;
        colorActivator->data["activated"] = 0;
        ofLog() << "hiding panel";
        
    }
    
    bool loadSettings() {
        ofLog() << motorSettings.load("motor_settings.xml");
        ofLog() << "loop length:" << motorSettings.getAttribute("loop", "length", 100);
        
        settings.clear();
        return settings.load("settings.xml");
    }
    
    void saveSettings() {
        settings.clear();
        int counter = 0;
        for (auto i: pickers) {
            settings.addTag("picker");
            settings.addAttribute("picker", "size", i->size.x, counter);
            settings.addAttribute("picker", "position_x", i->position.x, counter);
            settings.addAttribute("picker", "position_y", i->position.y, counter);
            settings.addAttribute("picker", "inside", i->data["inside"].asString(), counter);
            settings.addAttribute("picker", "threshold", i->data["threshold"].asFloat(), counter);
            settings.addAttribute("picker", "r", i->data["r"].asInt(), counter);
            settings.addAttribute("picker", "g", i->data["g"].asInt(), counter);
            settings.addAttribute("picker", "b", i->data["b"].asInt(), counter);
            counter++;
        }
        
        settings.save("settings.xml");
        
        
        ofFile file;
        file.open("serial_devices.txt", ofFile::WriteOnly, false);
        for(int k = 0; k < serial.getDeviceList().size(); k++){
            file.writeFromBuffer(ofBuffer(ofToString(k) + " - " + serial.getDeviceList()[k].getDeviceName() + "\n"));
        }

        file.close();
        /*
         {   "rotate": { "id": 1, "pos": [170, 125, 85, 50, 120]},
         "finger2": { "id": 3, "pos": [40, 70]},
         "finger1": { "id": 2, "pos": [50, 70]},
         "motor": { "id": [8, 7], "step": 20}
         }
         */
        
        motorSettings.clear();
        
        /*
        motorSettings.addTag("rotate");
        motorSettings.addAttribute("rotate", "1", 170, 0);
        motorSettings.addAttribute("rotate", "2", 125, 0);
        motorSettings.addAttribute("rotate", "3", 85, 0);
        motorSettings.addAttribute("rotate", "4", 50, 0);
        motorSettings.addAttribute("rotate", "5", 120, 0);
        motorSettings.addTag("finger2");
        motorSettings.addAttribute("finger2", "pos1", 40, 0);
        motorSettings.addAttribute("finger2", "pos2", 70, 0);
        motorSettings.addTag("finger1");
        motorSettings.addAttribute("finger1", "pos1", 50, 0);
        motorSettings.addAttribute("finger1", "pos2", 70, 0);
        motorSettings.addTag("delays");
        motorSettings.addAttribute("delays", "delay1", 100, 0);
        motorSettings.addAttribute("delays", "delay2", 300, 0);
        motorSettings.save("motor_settings.xml");
         */
    }
    
    int modulus (int arg) { if (arg < 0) return -arg; else return arg;}
    
    void sendMessageToHand(char motor, char value) {
        /*
        function tweenHand(motor, value) {
            
            var message = new Buffer(String.fromCharCode(0xAA, motor, value), "ascii");
            serialPort.write(message);
        */
        serial.writeByte(0xAA);
        serial.writeByte(motor);
        serial.writeByte(value);
    }
    ofSerial serial;
    
    double timelineStarted = 0;
    double timelineLength = 300;
    int phase = 0;
    int nextAction = 0;
    
    int getActivatedPickerIndex() {
        for (auto i = 0; i < pickers.size(); i++) {
            if (pickers[i]->data["worked"].asInt() == 1)
                return i;
        }
        return -1;
    };
    
    void serialTimeline() {
        int timePassed = ofGetElapsedTimeMillis() - timelineStarted;
        
        if ($("loopcheckbox")->data["play"].asInt() != 1)
              return;
        
        if ((timePassed) > motorSettings.getAttribute("loop", "length", 500))
        { // new loop
            timelineStarted = ofGetElapsedTimeMillis();
            phase = 0;
            string action = "";
            int index = getActivatedPickerIndex();
            if (index != -1) {
                action = pickers[getActivatedPickerIndex()]->data["inside"].asString();
                //ofLog() << "ACTIVATED:" << action;
            }
            ofLog() << "NEW LOOP TO PUSH BUTTON " << action;
            if (nextAction != 6) {
                nextAction = 0;
            }
            if (action == "1")
                nextAction = 1;
            if (action == "2")
                nextAction = 2;
            if (action == "3")
                nextAction = 3;
            if (action == "4")
                nextAction = 4;
            if (action == "5")
                nextAction = 5;
            if (action == "6")
                nextAction = 6;
            return;
        }
        
        if (phase == 0) {
            switch(nextAction) {
                case 1: sendMessageToHand(0x01, motorSettings.getAttribute("rotate", "n1", 500));
                    break;
                case 2: sendMessageToHand(0x01, motorSettings.getAttribute("rotate", "n2", 500));
                    break;
                case 3: sendMessageToHand(0x01, motorSettings.getAttribute("rotate", "n3", 500));
                    break;
                case 4: sendMessageToHand(0x01, motorSettings.getAttribute("rotate", "n4", 500));
                    break;
                case 5: sendMessageToHand(0x01, motorSettings.getAttribute("rotate", "n5", 500));
                    break;
                case 6: sendMessageToHand(0x01, motorSettings.getAttribute("rotate", "n5", 500));
                    break;
                
            };
            phase++;
            ofLog() << "...phase1 finished...";
            //ofLog() << "n1 = " << motorSettings.getAttribute("rotate", "n1", 500);
            //ofLog() << "n2 = " << motorSettings.getAttribute("rotate", "n2", 500);
        }
        
        if ((phase == 1) && (timePassed > motorSettings.getAttribute("delays", "delay1", 300, 0))) {
            switch(nextAction) {
                case 1: sendMessageToHand(0x03, motorSettings.getAttribute("finger1", "pos1", 500));
                    break;
                case 2: sendMessageToHand(0x03, motorSettings.getAttribute("finger1", "pos1", 500));
                    break;
                case 3: sendMessageToHand(0x03, motorSettings.getAttribute("finger1", "pos1", 500));
                    break;
                case 4: sendMessageToHand(0x03, motorSettings.getAttribute("finger1", "pos1", 500));
                    break;
                case 5: sendMessageToHand(0x02, motorSettings.getAttribute("finger2", "pos1", 500));
                    break;
                case 6: sendMessageToHand(0x02, motorSettings.getAttribute("finger2", "pos1", 500));
                    break;
                    
            };
            phase++;
            ofLog() << "...phase2 finished...";
        }

        if ((phase == 2) && (timePassed > (motorSettings.getAttribute("delays", "delay1", 300, 0) + motorSettings.getAttribute("delays", "delay2", 300, 0)))) {
            switch(nextAction) {
                case 1: sendMessageToHand(0x03, motorSettings.getAttribute("finger1", "pos2", 500));
                    break;
                case 2: sendMessageToHand(0x03, motorSettings.getAttribute("finger1", "pos2", 500));
                    break;
                case 3: sendMessageToHand(0x03, motorSettings.getAttribute("finger1", "pos2", 500));
                    break;
                case 4: sendMessageToHand(0x03, motorSettings.getAttribute("finger1", "pos2", 500));
                    break;
                case 5: sendMessageToHand(0x02, motorSettings.getAttribute("finger2", "pos2", 500));
                    break;
                case 6: sendMessageToHand(0x02, motorSettings.getAttribute("finger2", "pos2", 500));
                    break;
                    
            };
            phase++;
            ofLog() << "...phase3 finished...";
        }
    };
    /////
    
    vector<string> serialDevices;
    int selectedDevice;
		
    ofVideoGrabber grabber;
    
    Object *canvas, *video, *panel, *input, *colorActivator;
    Slider *slider, *threshold;
    DragDispatcher *dragD;
    
    vector<Object*> pickers;
    
    ofTrueTypeFont font1, font2;
    
    
    ofxXmlSettings settings, motorSettings;
    //ofPixelsRef pixelsRef;
};
