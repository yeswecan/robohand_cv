#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    loadSettings();

    ofLog() << "camera deviceid = " << motorSettings.getAttribute("camera", "deviceid", 99);
    grabber.listDevices();
    grabber.setDeviceID(motorSettings.getAttribute("camera", "deviceid", 0));
    grabber.initGrabber(640, 480);
    
    serial.listDevices();
    /* */
    serial.setup(motorSettings.getAttribute("serial", "deviceid", 0), 115200);
    selectedDevice = motorSettings.getAttribute("serial", "deviceid", 0);
    
    ofSleepMillis(2000);
    sendMessageToHand(0x03,
                      motorSettings.getAttribute("finger1", "pos1", 500));
    ofSleepMillis(500);
    sendMessageToHand(0x03,
                      motorSettings.getAttribute("finger2", "pos1", 500));
//    ofSleepMillis(2000);
//    sendMessageToHand();
    
    font1.loadFont("DaxlineProRegular.ttf", 35);
    font2.loadFont("DaxlineProRegular.ttf", 20);
    
    canvas = new Object("canvas", ofPoint(0,0), ofPoint(ofGetWidth(), ofGetHeight()));
    video = canvas->addChild(new Object("video", ofPoint(300, 0), ofPoint(640, 480), 4));
    
    video->draw = [&](Object *o) {
        if (dragD->isDraggingNow(o))
            ofSetColor(255, 255 - 155 * o->timeQFromLastTouch(600));
        else
            ofSetColor(255);
        grabber.draw(0, 0, o->size.x, o->size.y);
    };
    video->update = [&](Object *o) {
        if (!dragD->isDraggingNow(o)) {
            if (
                (video->parent->size.x - video->position.x - video->size.x)
                < (video->position.x))
                    video->position.animateTo(ofPoint(video->parent->size.x - video->size.x, 0), 600);
                else {
                    video->position.animateTo(ofPoint(0,0), 600);
                }
        }
    };
    video->addCallback(Object::TOUCH_DOWN, [&](Object *o) {
        for (auto &i: pickers) {
            i->data["selected"] = 0;
        }
        hidePanel();
    });
    
    
    dragD = (DragDispatcher*)video->addChild(new DragDispatcher());
    dragD->addDraggable(video, 0);
    dragD->setStrict(video, 0);
    
    function<void(Object*)> setupNewPicker = [&](Object *newPicker) {
        newPicker->draw = [&](Object *o) {
            ofSetColor(0);
            ofRect(-2, -2, o->size.x + 4, o->size.y + 4);
            
            ofSetColor(255, 200);
            ofRect(-1, -1, o->size.x + 2, o->size.y + 2);
            
            if (o->data["selected"].asInt() == 1)
                ofRect(-3, -3, o->size.x + 6, o->size.y + 6);
            
            ofSetColor(o->color.asColor());
            ofRect(0,0, o->size.x, o->size.y);
            
            
            ofSetColor(0);
            font2.drawString(o->data["inside"].asString(), 0, -9);
            ofSetColor(255);
            font2.drawString(o->data["inside"].asString(), 0, -10);

            if (o->data["worked"].asInt() == 1) {
                ofSetColor(255, 200 * sin(ofGetElapsedTimef() * 40));
                ofRect(0,0, o->size.x, o->size.y);
                ofSetColor(0, 200 * sin(ofGetElapsedTimef() * 40));
                font2.drawString(o->data["inside"].asString(), 0, -10);
            }
            /*
            ofSetColor(255);
            if (panel->data["appeared"] == 1)
                font2.drawString(ofToString(o->data["difference"].asInt()), 0, 50);
             */
        };
        
        newPicker->update = [&](Object *o) {
            ofPixelsRef pixelsRef = grabber.getPixelsRef();
            if (ofGetElapsedTimef() < 2) return;
            
            if (o->data["selected"].asInt() == 1) {
                o->size = ofPoint(slider->value * slider->scale);
                if (input->data["focused"].asInt() == 1) {
                    o->data["inside"] = input->data["inside"].asString();
                }
                o->data["threshold"] = threshold->value;
                o->data["r"] = colorActivator->color.asColor().r;
                o->data["g"] = colorActivator->color.asColor().g;
                o->data["b"] = colorActivator->color.asColor().b;
            }
            
            float r, g, b;
            for (auto i = 0; i < o->size.x; i++) {
                for (auto j = 0; j < o->size.y; j++) {
                    r += pixelsRef.getColor(o->position.x + i, o->position.y + j).r;
                    g += pixelsRef.getColor(o->position.x + i, o->position.y + j).g;
                    b += pixelsRef.getColor(o->position.x + i, o->position.y + j).b;
                }
            }
            float size = (o->size.x * o->size.y);
            ofColor color = ofColor(r / size, g / size, b / size);
            o->color = ofPoint(color.r, color.g, color.b);
            
            ///
            
            o->data["worked"] = 0;
            int difference = 0;
            difference += modulus (o->color.x - o->data["r"].asInt());
            difference += modulus (o->color.y - o->data["g"].asInt());
            difference += modulus (o->color.z - o->data["b"].asInt());
            o->data["difference"] = difference;
            if (difference < (o->data["threshold"].asFloat() * threshold->scale))
                o->data["worked"] = 1;
        };
        newPicker->data["selected"] = 0;
        
        newPicker->addCallback(Object::TOUCH_DOWN, [&](Object *o) {
            
            slider->value = o->size.x / slider->scale;
            threshold->value = o->data["threshold"].asFloat();
            input->data["inside"] = o->data["inside"].asString();
            colorActivator->color = ofPoint(o->data["r"].asInt(), o->data["g"].asInt(), o->data["b"].asInt());
            //ofLog() << "sending = " << o->data["sendind"].asString();
            
            showPanel();
            for (auto &i: pickers) {
                i->data["selected"] = 0;
            }
            o->data["selected"] = 1;
            return true;
        });
        
        dragD->addDraggable(newPicker, 0);
        dragD->setStrict(newPicker, 0);
    };
    
    if (loadSettings()) {
        ofLog() << "Settings found";
        for (auto i = 0; i < settings.getNumTags("picker"); i++) {
            pickers.push_back(video->addChild(new Object("picker" + ofToString(i), ofPoint(settings.getAttribute("picker", "position_x", 0, i), settings.getAttribute("picker", "position_y", 0, i)),
                ofPoint(settings.getAttribute("picker", "size", 0, i),
                        settings.getAttribute("picker", "size", 0, i)))));
            setupNewPicker(pickers.back());
            pickers[i]->data["inside"] = settings.getAttribute("picker", "inside", "?", i);
            pickers[i]->data["threshold"] = settings.getAttribute("picker", "threshold", 0.1, i);
            pickers[i]->data["r"] = settings.getAttribute("picker", "r", 0, i);
            pickers[i]->data["g"] = settings.getAttribute("picker", "g", 0, i);
            pickers[i]->data["b"] = settings.getAttribute("picker", "b", 0, i);
            ofLog() << pickers[i]->data["inside"].asString();
            
        }
    } else ofLog() << "SETTINGS NOT FOUND";
    if (pickers.size() < PICKER_COUNT)
    for (auto i = 0; pickers.size() < PICKER_COUNT; i++) {
        pickers.push_back(video->addChild(new Object("picker" + ofToString(i), ofPoint(ofRandom(video->size.x - 20), ofRandom(video->size.y - 20)),ofPoint(20, 20))));
        setupNewPicker(pickers.back());
        pickers.back()->data["inside"] = "K";
        pickers.back()->data["threshold"] = 0.1;
    }
    
    panel = canvas->addChild(new Object("panel", ofPoint(300, 0), ofPoint(300, 100), 2, true));
    panel->draw = [&](Object *o) {
        /*
        for(int k = 0; k < serial.getDeviceList().size(); k++){
            ofSetColor(255);
            font2.drawString(serial.getDeviceList()[0].getDeviceName(), 50, 150 + k * 25);
        }
         */
        
        ofSetColor(255, 200);
        ofRect(0, 0, o->size.getAnimated().x, o->size.getAnimated().y);
        ofSetColor(0, 200);
        ofRect(2, 2, o->size.getAnimated().x-4, o->size.getAnimated().y-4);
        
        ofSetColor(255, 255);
        if (panel->data["appeared"].asInt() == 1) {
            font1.drawString("robohand", 50, 63 - 150 * ( panel->size.getAnimationPhase()));
            font2.drawString("Trigger settings", 50, -135 + 200 * (panel->size.getAnimationPhase()));
        }
        else {
            font1.drawString("robohand", 50, 63 - 150 * ( 1 - panel->size.getAnimationPhase()));
            font2.drawString("Trigger settings", 50, -135 + 200 * (1 - panel->size.getAnimationPhase()));
        }
        
        
        ofSetColor(255);
        font2.drawString("Size:", 20, 185);
        font2.drawString("Threshold:", 20, 250);
        font2.drawString("Command:", 20, 315);
        if (colorActivator->data["activated"].asInt() == 0)
            font2.drawString("Color activator:", 20, 370);
        else font2.drawString("Select color:", 20, 370);
    };
    
    panel->data["appeared"] = 0;
    
    colorActivator = panel->addChild(new Object("color_activator", ofPoint(210, 340), ofPoint(70, 70)));
    colorActivator->draw = [&] (Object *o) {
        ofSetColor(255);
        if (o->data["activated"].asInt() == 1)
            ofSetColor(255 * sin(ofGetElapsedTimef() * 100));
        ofRect(-2, -2, o->size.x + 4, o->size.y + 4);
        
        ofSetColor(o->color.asColor());
        ofRect(0, 0, o->size.x, o->size.y);
    };
    colorActivator->touchDownC = [&] (Object *o) -> bool {
        o->data["activated"] = 1;
    };
    
    slider = (Slider*)panel->addChild(new Slider("sizeSlider", ofPoint(100, 150), ofPoint(160, 50), dragD));
    slider->font = &font2;
    
    threshold = (Slider*)panel->addChild(new Slider("sizeSlider", ofPoint(150, 215), ofPoint(118, 50), dragD));
    threshold->font = &font2;
    threshold->captionAddon = "";
    threshold->scale = 150;
    
    input = panel->addChild(new Object("input", ofPoint(160, 280), ofPoint(110, 50)));
    input->data["inside"] = "p";
    input->touchDownC = [&](Object *o)->bool {
        o->data["focused"] = 1;
        return true;
    };
    input->draw = [&](Object *o) {
        if (o->data["focused"].asInt() == 1) {
            ofSetColor(255);
            ofRect(-2, -2, o->size.x + 2, o->size.y + 2);
        }
        
        ofSetColor(100, 200);
        ofRect(0, 0, o->size.x - 2, o->size.y - 2);
        
        ofSetColor(255);
        if (o->data["focused"].asInt() == 1)
            font2.drawString(input->data["inside"].asString()
                         + (sin(ofGetElapsedTimef() * 7) > 0 ? "I":""), 10, 35);
        else font2.drawString(input->data["inside"].asString(), 10, 35);
    };
    
    Object *loopCheckbox = canvas->addChild(new Object("loopcheckbox", ofPoint(30, 500), ofPoint(220, 70)));
    loopCheckbox->data["play"] = 1;
    loopCheckbox->draw = [&](Object *o) {
        if (o->data["play"].asInt() == 1) {
            ofSetColor(0, 100);
            ofRect(0, 0, o->size.x, o->size.y);
            ofSetColor(255);
            font2.drawString("Play game? Yes", 10, 47);
        }
        else {
            ofSetColor(0, 150 + 50 * sin(ofGetElapsedTimef()));
            ofRect(0, 0, o->size.x, o->size.y);
            ofSetColor(255);
            font2.drawString("Play game? No", 10, 47);
        }
    };
    loopCheckbox->touchDownC =[&](Object *o)->bool {
            if (o->data["play"].asInt() == 1)
                o->data["play"] = 0;
            else o->data["play"] = 1;
        return true;
    };
}

//--------------------------------------------------------------
void ofApp::update(){
    ofShowCursor();
    
    serialTimeline();
    
    grabber.update();
    
    Animator::animator().update();
    canvas->updateCycle();

    if ((ofGetElapsedTimef() > 2) && (ofGetElapsedTimef() < 2.5)) {
        $("panel")->zIndex = 2;
        $("video")->zIndex = 3;
    }

    if (($("panel")->position.getAnimationPhase() == 1) || ($("panel")->position.getAnimationPhase() == 0))
    if (video->position.x > (ofGetWidth() - video->position.x - video->size.x)) {
        $("panel")->position.animateTo(ofPoint(0, $("panel")->position.getAnimated().y), 500);
        float panelwidth = (video->position.x);
        if (!$("panel")->size.isAnimatingNow())
            $("panel")->size.animateTo(ofPoint(panelwidth, $("panel")->size.getAnimated().y), 300);
    } else {
        float panelwidth = (ofGetWidth() - video->size.x - video->position.x);
        $("panel")->size.x = panelwidth;
        $("panel")->position.animateTo(ofPoint(video->position.x + video->size.x, $("panel")->position.getAnimated().y), 500);
        if (!$("panel")->size.isAnimatingNow())
            $("panel")->size.animateTo(ofPoint(panelwidth, $("panel")->size.getAnimated().y), 300);
    }
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    canvas->drawCycle();
    //pixelsRef = grabber.getPixelsRef();
    
    ofSetColor(255);
    ofRect(0, ofGetHeight() - 30, ofGetWidth() * (((float)ofGetElapsedTimeMillis() - (float)timelineStarted)) / motorSettings.getAttribute("loop", "length", 500), 30);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (input->data["focused"].asInt() == 1) {
        if (key == 127) {
            if (input->data["inside"].asString().length() > 0)
            input->data["inside"] = input->data["inside"].asString().erase(input->data["inside"].asString().length() - 1);
        } else
        if (key == 13) {
            input->data["focused"] = 0;
        } else
            input->data["inside"] = input->data["inside"].asString() + (char)key;
    }
    if (key == 32)
        $("loopcheckbox")->touchDownC($("loopcheckbox"));
    ofLog() << key;
    
//    sendMessageToHand();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    if (colorActivator->data["activated"].asInt() == 1)
    {
        ofPoint point = (ofPoint(x,y) - video->transformedPosition);
        point.x = ofClamp(point.x, 0, 639);
        point.y = ofClamp(point.y, 0, 479);
        ofPixelsRef pixelsRef = grabber.getPixelsRef();
        colorActivator->color = ofPoint(pixelsRef.getColor(point.x, point.y).r, pixelsRef.getColor(point.x, point.y).g, pixelsRef.getColor(point.x, point.y).b);
    }
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
//    ofLog() << "BROADCASTING TOUCHDRAG WITH " << x << ":" << y;
    canvas->touchBroadcast(ofPoint(x,y), Object::DRAG);
//    canvas->touchPoint = ofPoint(x,y);
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
//    ofLog() << "BROADCASTING TOUCHDOWN WITH " << x << ":" << y;
    if (colorActivator->data["activated"] == 1) {
        colorActivator->data["activated"] = 0;
    } else
    canvas->touchBroadcast(ofPoint(x,y), Object::DOWN);
//    canvas->touchPoint = ofPoint(x,y);
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    canvas->touchBroadcast(ofPoint(x,y), Object::UP);
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    canvas->size = ofPoint(w,h);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
