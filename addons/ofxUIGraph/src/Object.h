
// Object.h is an experiment for upcoming UI library.
// (c) Lyosha Zebra, 2014

///
#define DEFAULT_EASING_FACTOR 5
///////////////////////

#include "ofxJSONElement.h"
#include "ofxXMLSettings.h"
#include "ofMain.h"
#include <tr1/functional>
#include "strokeDraw.h"

// WARNING: this macro is a temporary one. Replace it when we'll migrate to libc++ on iOS
#define function tr1::function
//

class Animator {
public:
    struct Anim {
        double started, time;
        double (*easing) (double arg);
    };
    
    vector<Anim*> animations;
    
    double (*defaultEasing)(double arg) = [](double arg) {
        return (
        pow(arg, DEFAULT_EASING_FACTOR) / (pow(arg, DEFAULT_EASING_FACTOR) + pow(1 - arg, DEFAULT_EASING_FACTOR))
        );
    };

    double (*quadOutEasing)(double arg) = [](double arg) {
        return (
                pow(arg, 3)
                );
    };
    
    
    Anim* addAnimation(double start, double time) {
        Anim* anim = new Anim; anim->time = time;
        anim->started = start;
        animations.push_back(anim);
        
        // testing lambda for easing
        anim->easing = defaultEasing;        //
        return anim;
    };
    
    Anim* addAnimation(double time) {
        Anim* anim = new Anim; anim->time = time;
        anim->started = ofGetElapsedTimeMillis();
        animations.push_back(anim);
        
        // testing lambda for easing
        anim->easing = defaultEasing;        //
        return anim;
    };
    
    void eraseAnimation(Anim *arg) {
        std::vector<Anim*>::iterator position = std::find(animations.begin(), animations.end(), arg);
        if (position != animations.end()) // == vector.end() means the element was not found
            animations.erase(position);
    }
    
    bool isAnimating(Anim *arg) {
        std::vector<Anim*>::iterator position = std::find(animations.begin(), animations.end(), arg);
        if (position != animations.end()) {
            return true;
        } else return false;
    }
    
    // TODO: no way it should return NULL. it's not pointer, so we confuse 0 and NULL with
    // no profit whatsoever. it's awful. we need some other way to tell user there's no such animation on stack
    double getAnimation(Anim *arg) {
        if(isAnimating(arg)) {
            
            double result = ((millis - arg->started) / arg->time);
            return arg->easing(result);
            
            //return 1;
        } else return NULL;
    }
    
    unsigned long long millis;
    
    void update() {
        millis = ofGetElapsedTimeMillis();
                for (auto i: animations) {
                    if ((ofGetElapsedTimeMillis() - i->started) > i->time) {
                        // delete animation
                        eraseAnimation(i);
                    };
                }
        //            ofLog() << i.;
        //        }
        // ofLog() << "animations:" << animations.size();
    }
    
    static Animator & animator() {
        static Animator animator;
        return animator;
    }

};


class UIController {
public:
    virtual void UIEvent (string arg) {
        
    }
};

class ofPointF: public ofPoint {
public:
    template<typename T>
    ofPointF(T x, T y) : ofPoint (x, y) {};
    
    template<typename T>
    ofPointF(T x, T y, T z) : ofPoint (x, y, z) {};

    ofPointF() { }
    //    ofPointF(T position) : ofPoint (position) { };

    // TODO: Fix the broken copy constructor
    ofPointF& operator=(const ofPoint& arg) {
        x = arg.x; y = arg.y; z = arg.z;
    };
    ofPointF& operator=(const ofPointF& arg) {
        x = arg.x; y = arg.y; z = arg.z;
        anim = arg.anim;
        previous = arg.previous;
    };
    
    void setEasing(double (*easing)(double arg)) {
        anim->easing = easing;
    }
    
    void startAnimation(const ofPointF startPoint, const ofPointF endPoint, double startTime, double animationTime) {
//        Animator::Anim *new_anim;
        previous.x = startPoint.x;
        previous.y = startPoint.y;
        previous.z = startPoint.z;
        x = endPoint.x;
        y = endPoint.y;
        z = endPoint.z;
        anim = Animator::animator().addAnimation(startTime, animationTime);
    };
    
    void startAnimation(const ofPointF startPoint, const ofPointF endPoint, Animator::Anim *anim_) {
        //        Animator::Anim *new_anim;
        previous.x = startPoint.x;
        previous.y = startPoint.y;
        previous.z = startPoint.z;
        x = endPoint.x;
        y = endPoint.y;
        z = endPoint.z;
        anim = anim_;
    };
    
    bool isAnimatingNow() {
        return Animator().animator().isAnimating(anim);
    }
    
    void animateTo(const ofPoint endPoint, long time) {
        if ((endPoint.x == x) && (endPoint.y == y))
            return;
        ofLog() << "starting vector animation " << " from " << ofPoint(x,y)<< " to " << endPoint.x << " , " << endPoint.y;
        previous = getAnimated();
        x = endPoint.x; y = endPoint.y; z = endPoint.z;
        if (!doesAnimation())
            anim = Animator::animator().addAnimation(ofGetElapsedTimeMillis(), time);
        else {
            anim->started = ofGetElapsedTimeMillis();
            anim->time = time;
        }
    }

    void attachAnimation (Animator::Anim *anim_) {
        anim = anim_;
    }
    
    double getAnimationPhase() {
        double value = Animator().animator().getAnimation(anim);
        double animValue = (value == NULL? 1 : value);
        return animValue;
    }
    
    bool inside(ofPoint first, ofPoint second) {
        //ofLog() << "x:" << x << ", y:" << y;
        //ofLog() << "first x:" << first.x << ", first y:" << first.y;
        //ofLog() << "s x:" << second.x << ", s y:" << second.y;
        if ((x >= first.x) && (x <= second.x)
            && (y >= first.y) && (y <= second.y)
            ) {
            return true;
        } else return false;
    }

    bool inside(int first, int second) {
        //ofLog() << "x:" << x << ", y:" << y;
        //ofLog() << "first x:" << first.x << ", first y:" << first.y;
        //ofLog() << "s x:" << second.x << ", s y:" << second.y;
        if ((x >= first) && (x <= second)) {
            return true;
        } else return false;
//        if ((x >= first) && (x <= second)) {
//            return true;
//        } else return false;
    }
    
    bool doesAnimation() {
        return Animator().animator().isAnimating(anim);
    }
    
    ofPoint getAnimated() {
        double animValue;
        if (doesAnimation())
            animValue = Animator().animator().getAnimation(anim);
        else animValue = 1;
        //double animValue = (value == NULL? 1 : value);

        ofPoint result = ofPoint(this->previous.x * (1 - animValue) + this->x * animValue,
                                 this->previous.y * (1 - animValue) + this->y * animValue,
                                 this->previous.z * (1 - animValue) + this->z * animValue);
        return result;
    }
    
    ofColor asColor() {
        double value = Animator().animator().getAnimation(anim);
        double animValue = (value == NULL? 1 : value);
        
        ofColor result = ofColor( this->previous.x * (1 - animValue) + this->x * animValue,
                                 this->previous.y * (1 - animValue) + this->y * animValue,
                                 this->previous.z * (1 - animValue) + this->z * animValue);
        return result;
    }
    
    Animator::Anim* animationObject() { return anim;}
    
protected:
    bool isAnimating;
    Animator::Anim *anim;
    
private:
    ofPoint previous;
};


class Object {
public:
    //////// ***********  CALLBACKS  ***********  //////////
    // TODO: a vector of generic callbacks with a list of possible
    // events to react to.
    
    /*           Replace this with your own lambdas            */
    
    function<void(Object*)> draw = [this](Object *obj){
        /*
        ofPoint mySize = size.getAnimated();
        ofSetColor(255, 200);
        ofRect(-2,-2,mySize.x+4, mySize.y+4);
        ofSetColor(this->color.asColor(), 200);
        ofRect(0,0,mySize.x, mySize.y);
        ofSetColor(255);
        ofDrawBitmapString(name + ". children:" + ofToString(children.size()) + "\n parent name:" + parent->name, 5, 15);
         */
    };

    function<void(Object*)> update = [](Object *obj){
    };
    
    function<void()> drawWithoutContext = [this](){
        /*
        ofPoint mySize = size.getAnimated();
        ofSetColor(255, 200);
        ofRect(-2,-2,mySize.x+4, mySize.y+4);
        ofSetColor(this->color.asColor(), 200);
        ofRect(0,0,mySize.x, mySize.y);
        ofSetColor(255);
         */
        /*ofDrawBitmapString(name + ". children:" + ofToString(children.size()) + "\n parent name:" + parent->name, 5, 15);*/
    };
    
    function<void()> updateWithoutContext = []() {};
    
    function<bool()> touchDown = []()->bool {
        return false;
    };
    function<bool()> touchMove = []()->bool {
        return false;
    };
    function<bool()> touchUp = []()->bool {
        return false;
    };
    
    function<bool(Object*)> touchDownC = [](Object *o)->bool {
        return false;
    };

    function<bool(Object*)> touchUpC = [](Object *o)->bool {
        return false;
    };

    function<bool(Object*)> touchMoveC = [](Object *o)->bool {
        return false;
    };

    
    /*              Common callbacks                    */
    
    enum eventType {
        TOUCH_UP, TOUCH_DOWN, TOUCH_DRAG, MOUSE_MOVE, MOUSE_UP , UPDATE, DRAW
    };
    // TODO: add additional callback struct with parameters,
    // e.g. touch point, etc
    struct commonCallback {
        function<void(Object*)> callback;
        eventType ccase;
        string cid;
    };
    vector <commonCallback> commonCallbacks;
    
    void addCallback(eventType ccase, function<void(Object*)> callback) {
        commonCallback addon;
        addon.ccase = ccase; addon.callback = callback;
        commonCallbacks.push_back(addon);
    };
    
    void addCallback(eventType ccase, function<void(Object*)> callback, string cid) {
        commonCallback addon;
        addon.ccase = ccase; addon.callback = callback;
        addon.cid = cid;
        commonCallbacks.push_back(addon);
    };
    
    void executeCallbacks(eventType ccase, Object *o) {
        registerEvent(ccase);
        for (auto i: commonCallbacks) {
            if (i.ccase == ccase)
                i.callback(o);
        }
    };
    
/*
    bool (*touchMove)(Object *, UIController *) = [](Object *object, UIController *controller)->bool {
        return false;
    };
    
    bool (*touchUp)(Object *, UIController *) = [](Object *object, UIController *controller)->bool {
        return false;
    };*/
    
    /*                      Class routines                     */
    
    Object* addChild(Object *object) {
        object->parent = this;
        object->topParent = topParent;
        if (object->zIndex == 0)
            object->zIndex = zIndex++;
        //ofLog() << "adding " << object->name  << "(" << object->zIndex << ")" << " to " << name << "(" << zIndex << ")";
        children.push_back(object);
        //ofLog() << "result - " << children.back()->name << "(" << children.back()->zIndex << ")";
        object->zIndex = 52;
        return children.back();
    };
    
    Object* getChild(string iD) {
        for (auto i: children) {
            if (i->name == iD) return i;
            if (i->getChild(iD) != NULL) return i->getChild(iD);
        }
        return NULL;
    }
    
    void gotoState(string stateId, int time) {
        ofLog() << name << " wants to go to state " << stateId;
        ofLog() << "..current:" << currentState;
        if (stateId == currentState) return;
        if (data[stateId].isMember("position")) {
            ofLog() << name << " is ANIMATING POSITION";
            position.animateTo(ofPoint(
                               data[stateId]["position"].isMember("x") ? data[stateId]["position"]["x"].asInt() : position.x,
                               data[stateId]["position"].isMember("y") ? data[stateId]["position"]["y"].asInt() : position.y,
                               data[stateId]["position"].isMember("z") ? data[stateId]["position"]["z"].asInt() : position.z),
                                       time);
        }
        if (data[stateId].isMember("size")) {
            ofLog() << name << " is ANIMATING SIZE";
            size.animateTo(ofPoint(
                                       data[stateId]["size"].isMember("x") ? data[stateId]["size"]["x"].asInt() : position.x,
                                       data[stateId]["size"].isMember("y") ? data[stateId]["size"]["y"].asInt() : position.y,
                                       data[stateId]["size"].isMember("z") ? data[stateId]["size"]["z"].asInt() : position.z),
                               time);
        }
        previousState = currentState;
        currentState = stateId;
        stateAnim = Animator::animator().addAnimation(time);
    }
    
    void addState(string name, ofPoint position_) {
        data[name] = Json::objectValue;
        data[name]["position"] = Json::objectValue;
        data[name]["position"]["x"] = position_.x;
        data[name]["position"]["y"] = position_.y;
        data[name]["position"]["z"] = position_.z;
        
    };
    
    double getPhaseForState(string stateId) {
        // TODO: sometimes it gives 1 when the result should be 0.
        // objects are blinking because of it
        double result = Animator::animator().getAnimation(stateAnim);
        if (stateId == currentState) {
            if (result != NULL)
                return result;
            else return 1;
        }
        if (stateId == previousState) {
            if (result != NULL)
                return (1 - result);
            else return 0;
        }
        return 0;
    }
    
    void addXML(ofxXmlSettings *addition, Object *targetParent) {
        //ofLog() << "addxml called - " << addition->getNumTags("object") << " objects. pushlevel:" << addition->getPushLevel() ;
        for (auto i = 0; i < addition->getNumTags("object"); i++) {
            ofLog() << "object " << addition->getAttribute("object", "name", "<unknown>", i);
            Object *addon = targetParent->addChild(new Object());
            addon->name = addition->getAttribute("object", "name", "<unknown>", i);
            addon->zIndex = addition->getAttribute("object", "z-index", 0, i);
            if (addition->getAttribute("object", "z-index", 999, i) == 999) {
               if (targetParent != nullptr)
                   addon->zIndex = targetParent->zIndex;
            };
            string defaultState = addition->getAttribute("object", "default_state", "default", i);
            // tree structure support:
            addition->pushTag("object", i);
            
            if (addition->getNumTags("position") > 0) {
                ofLog() << ".. position found:" << addition->getAttribute("position", "x", 0) << "x" << addition->getAttribute("position", "y", 0);
                addon->position = ofPoint(addition->getAttribute("position", "x", 0), addition->getAttribute("position", "y", 0));
            }
            if (addition->getNumTags("bounds") > 0) {
                addon->position = ofPoint(addition->getAttribute("bounds", "x", 0), addition->getAttribute("bounds", "y", 0));
                addon->size = ofPoint(addition->getAttribute("bounds", "w", 0), addition->getAttribute("bounds", "h", 0));
            }
            if (addition->getNumTags("size") > 0) {
                ofLog() << ".. size found:" << addition->getAttribute("size", "x", 0) << "x" << addition->getAttribute("size", "y", 0);
                addon->size = ofPoint(addition->getAttribute("size", "x", 0), addition->getAttribute("size", "y", 0));
            }
            
            for (auto j = 0; j < addition->getNumTags("state"); j++) {
                ofLog() << ".....state " << addition->getAttribute("state", "name", "", j);
                string stateName = addition->getAttribute("state", "name", "", j);
                addon->data[stateName] = Json::objectValue;
                addition->pushTag("state", j);
                if (addition->getNumTags("position") > 0) {
                    addon->data[stateName]["position"] = Json::objectValue;
                    addon->data[stateName]["position"]["x"] = addition->getAttribute("position", "x", 0);
                    addon->data[stateName]["position"]["y"] = addition->getAttribute("position", "y", 0);
                    
                    ofLog() << "   ::: position found:" << addon->data[stateName]["position"]["x"].asInt() << "x" << addon->data[stateName]["position"]["y"].asInt();
                }
                if (addition->getNumTags("size") > 0) {
                    addon->data[stateName]["size"] = Json::objectValue;
                    addon->data[stateName]["size"]["x"] = addition->getAttribute("size", "x", 0);
                    addon->data[stateName]["size"]["y"] = addition->getAttribute("size", "y", 0);
                    ofLog() << "   ::: size found:" << addon->data[stateName]["size"]["x"].asInt() << "x" << addon->data[stateName]["size"]["y"].asInt();
                }
                addition->popTag();
            }
            
            // test:
            addXML(addition, addon);
            
            addition->popTag();

            addon->gotoState(defaultState, 0);
            // loading children
//            addXML(<#ofxXmlSettings *addition#>, <#Object *parent#>)
        }
        
    }
    
    void parseXML(string filename) {
        ofxXmlSettings settings;
        settings.load(filename);
        addXML(&settings, this);
    }
    
    void parseJSON(string filename) {
        ofxJSONElement file; file.open(filename);
        for (auto i: file.getMemberNames()) { //top-level nodes are always Objects*
            Object *addon = new Object(ofPoint(0,0), ofPoint(0,0));
            addon->name = i;
            if (file[i].isMember("z-index"))
                addon->zIndex = file[i]["z-index"].asInt();
            if (file[i].isMember("states"))
                addon->data = file[i]["states"];
            if (file[i].isMember("size")) {
                addon->size = ofPoint(
                                      file[i]["size"].isMember("x") ? file[i]["size"]["x"].asInt() : 0,
                                      file[i]["size"].isMember("y") ? file[i]["size"]["y"].asInt() : 0,
                                      file[i]["size"].isMember("z") ? file[i]["size"]["z"].asInt() : 0
                );
            }
            children.push_back(addon);
            if (file[i].isMember("default_state")) {
                addon->gotoState(file[i]["default_state"].asString(), 0);
            }
        }
    }
    
    /*    Application work cycle (call this from OF)           */

    void updateCycle() {
        for (auto &i: children) {
            i->transformedPosition = transformedPosition + i->position.getAnimated() + innerTransform;
            i->touchPoint = touchPoint;
            i->touchingNow = touchingNow;
            i->updateCycle();
        }
        update(this);
        updateWithoutContext();
        executeCallbacks(Object::UPDATE, this);
    }
    
    void drawCycle() {
        draw(this);
        drawWithoutContext();
        executeCallbacks(Object::DRAW, this);
        
        int czIndex = 0;
        int childrenDrawn = 0;
        while (childrenDrawn < children.size()) {
            for (auto &i: children) {
                if (i->zIndex == czIndex) {
                    if ((clippingTest(i)) || (bypassClippingTest))
                    {
                        // Here we transform matrix FROM THE GROUND UP to
                        // support the inner logic of drawing
                        ofPushMatrix();
                        ofTranslate(i->position.getAnimated() + innerTransform.getAnimated());
                            if (i->useFbo) {
                                i->myFbo.begin();
                                    ofClear(0,0,0,0);
                                    i->drawCycle();
                                i->myFbo.end();
                                
                                ofSetColor(255);
                                i->myFbo.getTextureReference().drawSubsection(0, 0, i->size.getAnimated().x, i->size.getAnimated().y, 0, 0, i->size.getAnimated().x, i->size.getAnimated().y);
                            }
                            else {
                                    i->drawCycle();
                            }
                        ofPopMatrix();
                
                    } else {/*
                        ofLog() << "name:" << i->name << " . x:" << i->position.getAnimated().x << " , y:" << i->position.getAnimated().y;
                        */
                        ofPushMatrix();
                        ofTranslate(i->position.getAnimated() + innerTransform.getAnimated());
                            i->drawCycle();
                            ofSetColor(0, 200);
                            ofRect(0,0, i->size.x, i->size.y);
                            ofSetColor(255);
                            ofLine(0,0, i->size.x, i->size.y);
                            ofLine(0,i->size.y, i->size.x, 0);
                        ofPopMatrix();
                    }
                    childrenDrawn++;
                }
            }
            czIndex++;
        }
    }
    
    bool bypassClippingTest = false;
    
    // TODO: clipping test is too simple and not accurate in most
    // situations
    bool clippingTest(Object *i) {
        if ((parent!= NULL) && (!parent->clipTestChildren)) return true;
        if (!clipTestMe) return true;
        if (parent == NULL) return true;
        ofRectangle bounds = ofRectangle(- innerTransform.x, - innerTransform.y, parent->size.x, parent->size.y);
        
        if (
            (
             (i->position.getAnimated().x >= bounds.x) ||
             ((i->position.getAnimated().x + i->size.getAnimated().x) >= bounds.x))
            &&
            ((i->position.getAnimated().y >= bounds.y) ||
             ((i->position.getAnimated().y + i->size.getAnimated().y) >= bounds.y))
            && (i->position.getAnimated().x < (bounds.x + bounds.width))
            && (i->position.getAnimated().y < (bounds.y + bounds.height) ))
        {
            return true;
        } else return false;
    };
    
    
    
    ofFbo myFbo;
    bool useFbo = false;
    
    void setFboSize(ofPoint size) {
        // TODO: free resources first!
        myFbo.allocate(size.x, size.y);
    };
    
    bool innerViewport = false;
    ofPoint innerViewportSize;
    
    enum TouchType {DOWN, MOVE, DRAG, UP};
    
    int getMaxIndex() {
        int current_zIndex = 0, maxZIndex = zIndex;
        for (auto i: children) {
            //if (i->zIndex > 10) i->zIndex = 5;
            if (maxZIndex < i->zIndex) {
                maxZIndex = i->zIndex;
            }
        }
        return maxZIndex;
    };
    
    bool pointInclusionTest(Object *i, ofPoint point) {
        ofPoint position = point/* - innerTransform*/;
        if ((i->position.getAnimated().x < position.x) && (i->position.getAnimated().y < position.y) &&
            (position.x < (i->position.getAnimated().x + i->size.x)) &&
            (position.y < (i->position.getAnimated().y + i->size.y)))
            return true;
        else return false;
    }
    
    
    // TODO: re-check and fix "focused" mess
    vector<Object*> focused;
    
    // TODO: touchBroadcast and touchingNow should work
    // for multitouch surfaces AND mouse at the same time
    // AND be reasonably simple to use.
    // An architecture to match these criteria
    // is a must-have.
    bool touchingNow = false;
    ofPoint touchPoint; // this is meant to use in a top-level state container.
                        // not reasonable, it will be
                        // misunderstood, cause this way every
                        // object has it. bad idea.
                        // TODO: get rid of this! Better use some static stuff
    
    bool touchBroadcast(ofPoint position, TouchType type) {
        if (type == TouchType::UP)
            touchingNow = false;
        if (type == TouchType::DOWN)
            touchingNow = true;
        if (type == TouchType::DRAG)
            //ofLog() << "DRAG : " << touchPoint;
        
        touchPoint = touchPoint;
        /*
        ofLog() << "::" << name << " sets touchpoint to " << position << " in touch type " << (type == DOWN? "DOWN" : (type == UP? "UP" : (type == DRAG? "DRAG":"UNKNOWN")));
         */
        
        int counter = 0;
        int current_zIndex = 0, maxZIndex = zIndex;
        for (auto i: children) {
            //ofLog() << counter << ":" << i->name << " - " << i->zIndex;
            //if (i->zIndex > 10) i->zIndex = 5;
            if (maxZIndex < i->zIndex) {
                    maxZIndex = i->zIndex;
            }
            if (maxZIndex < i->getMaxIndex()) {
                maxZIndex = i->getMaxIndex();
            }
            counter++;
        }
        //ofLog() << "max zindex is " << maxZIndex << " name is " << test;
        current_zIndex = maxZIndex;
        
        touchPoint = position;
        
        bool result = false;
        while ((!result) && (current_zIndex >= 0) ) {
            for (auto i: children) {
                if ( (i->zIndex == current_zIndex) &&
                    (pointInclusionTest(i, position - innerTransform))
                    ) {
                        i->lastTouched = ofGetElapsedTimeMillis();
                        i->lastTouchedPosition = position - i->position - innerTransform;
                        /*
                        if (type == TouchType::DRAG) {
                            ofLog() << "DRAG INSIDE " << i->name << "AT " << i->lastTouchedPosition;
                        }*/
                        if (type == TouchType::DOWN) {
                            registerEvent(TOUCH_DOWN);
                            ofLog() << "touched DOWN " << i->name << " with zIndex = "<< current_zIndex;
                            if (!i->touchBroadcast(position - i->position, type)) {
                                if ((i->touchDown()) || (i->touchDownC(i))) {
                                    ofLog() << " --- touch down ended up inside " << i->name;
                                    i->executeCallbacks(Object::TOUCH_DOWN, i);
                                    return true;
                                }
                            } else {
                                ofLog() << "touch down ended up inside children of " << i->name;
                                return true;
                            }
                        }
                    
                        if (type == TouchType::MOVE) {
                            registerEvent(TOUCH_DRAG);
                            
                            if (!i->touchBroadcast(position - i->position, type)) {
                                if ((i->touchMove()) || (i->touchMoveC(i))) {
                                    i->executeCallbacks(Object::MOUSE_MOVE, i);
                                    ofLog() << " --- touch move ended up inside " << i->name;
                                    return true;
                                }
                            } else {
                                ofLog() << "touch move ended up inside children of " << i->name;
                                return true;
                            }

                        }
                    
                        if (type == TouchType::UP) {
                            registerEvent(TOUCH_UP);
                            if (!i->touchBroadcast(position - i->position, type)) {
                                if ((i->touchUp()) || (i->touchUpC(i))) {
                                    i->executeCallbacks(Object::TOUCH_UP, i);

                                    i->executeCallbacks(Object::MOUSE_UP, i);
                                    ofLog() << " --- touch up ended up inside " << i->name;
                                    return true;
                                }
                            } else {
                                ofLog() << "touch move ended up inside children of " << i->name;
                                return true;
                            }
                        }
                    
                    }
            }
            //ofLog() << current_zIndex;
            current_zIndex --;
        }
        return false;
    }
    
    
    /*                  Helpers               */
    std::map <eventType, double> eventStack;
    
    void registerEvent(eventType type) {
        //ofLog() << "!!!! << " << name << " 's event stack size is " << eventStack.size();
        //ofLog() << "!!!!" << " previous :" << timeFromEvent(type);
        //ofLog() << "!!!!" << " previous " << (type == TOUCH_DOWN ? "TOUCH_DWN" : (type == TOUCH_UP ? "TOUCH_UP" : "??")) << " Q 500:" << timeQFromEvent(type, 500);
        //ofLog() << "!!!!" << name << " : REGISTERING " << (type == TOUCH_DOWN ? "TOUCH_DWN" : (type == TOUCH_UP ? "TOUCH_UP" : "??"));
        eventStack[type] = ofGetElapsedTimeMillis();
    }
    
    float timeFromEvent(eventType type) {
        return ofGetElapsedTimeMillis() - eventStack[type];
    };
    
    float timeQFromEvent(eventType type, int limitInMilliseconds) {
        if (eventStack.find(type) == eventStack.end())
            return 1; // no such event
        else return ofClamp(((timeFromEvent(type))/(float)limitInMilliseconds), 0, 1);
    }
    
    // TODO: timeQFromLastTouch and lastTouched should contain
    // and return all kinds of info FROM MULTIPLE SOURCES,
    // e.g. different fingers, mouse, whatever
    double timeQFromLastTouch(int limitInMilliseconds) {
        if (lastTouched > ofGetElapsedTimeMillis())
            return 1;
        if ((ofGetElapsedTimeMillis() - lastTouched) > limitInMilliseconds) return 1;
            else return ((double)(ofGetElapsedTimeMillis() - lastTouched) / (double)limitInMilliseconds);
    };
    
    int millisFromLastTouch() {
        return (ofGetElapsedTimeMillis() - lastTouched);
    }
    
    
    ofPoint getTranslatedPosition() {
            if (parent!=nullptr)
                return parent->getTranslatedPosition() + position.getAnimated();
            else return position.getAnimated();
    };
    
    ofPoint transformedPosition;
    ofPointF innerTransform;
    
    /*                Constructors            */
    
    Object () {
        color = ofPoint(ofRandom(255), ofRandom(255), ofRandom(255));
        topParent = this;
        parent = this;
    }
    
    Object (ofPoint pos, ofPoint siz) : Object() {
        position.x = pos.x; position.y = pos.y;
        size.x = siz.x; size.y = siz.y;
    }
    
    Object (string nam, ofPoint pos, ofPoint siz) : Object(pos, siz) {
        name = nam;
    }

    Object (string nam, ofPoint pos, ofPoint siz, int z_index) : Object(nam, pos, siz) {
        zIndex = z_index;
        ofLog() << "created object " << nam << " with zindex = " << zIndex;
    }

    Object (string nam, ofPoint pos, ofPoint siz, bool fbo) : Object(nam, pos, siz) {
        if (fbo)
            myFbo.allocate(ofGetWidth(), ofGetHeight());
        useFbo = fbo;
    }
    
    Object (string nam, ofPoint pos, ofPoint siz, int z_index, bool fbo) : Object(nam, pos, siz, z_index) {
        // WARNING!! Test case
        if (fbo)
            myFbo.allocate(ofGetWidth(), ofGetHeight());
        useFbo = fbo;
    }
    
    // TODO: some more constructors
    
    bool clipTestChildren = true; // clipTest checks roughly if the object's children is on screen not to
                   // render too much garbage
    bool clipTestMe = true;
    
    Object *topParent;
    Object* getTopParent() {return topParent;}
    string report() {
        return "object " + name + " with parent " + parent->name + " and zIndex = " + ofToString(zIndex);
    }
    
    Object *parent = nullptr;
    vector<Object*> children;
    ofxJSONElement data;
    long lastTouched = 99999999;
    ofPointF lastTouchedPosition;
    string name = "noname";
    
    bool isViewport;
    ofPoint viewportSize;
    
    int zIndex = 0;
    
    string currentState = "", previousState = "";
    Animator::Anim *stateAnim;
    
    ofPointF position, size;
    ofPointF color;
    
};


